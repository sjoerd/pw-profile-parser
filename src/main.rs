use anyhow::Result;
use clap::Parser;
use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

#[derive(Debug, Parser)]
struct Opts {
    input: PathBuf,
}

#[derive(Clone, Debug)]
struct Driver {
    audio_period: u64, // Audio period
    driver_end: u64,
    delay: u64,            // driver delay
    estimated_period: u64, // estimated period
}

#[derive(Clone, Debug)]
struct Follower {
    id: u64,
    signalled: u64,
    awake: u64,
    end: u64,
    duration: u64,
    latency: u64,
    status: u64,
    reserved: u64,
}

#[derive(Clone, Debug)]
struct Frame {
    driver: Driver,
    followers: Vec<Follower>,
}

struct ParseValues<'a> {
    remaining: &'a str,
}

fn parse(remaining: &str) -> ParseValues {
    ParseValues { remaining }
}

impl Iterator for ParseValues<'_> {
    type Item = Option<u64>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(e) = self.remaining.find("\t") {
            let data = &self.remaining[0..e];
            self.remaining = &self.remaining[e + 1..];
            if data.is_empty() || data == " " {
                Some(None)
            } else {
                Some(data.parse().ok())
            }
        } else {
            None
        }
    }
}

fn main() -> Result<()> {
    let opts = Opts::parse();
    let file = File::open(opts.input)?;
    let file = BufReader::new(file);
    for line in file.lines() {
        let line = line?;
        let mut parse = parse(&line);
        let driver = Driver {
            audio_period: parse.next().unwrap().unwrap(),
            driver_end: parse.next().unwrap().unwrap(),
            delay: parse.next().unwrap().unwrap(),
            estimated_period: parse.next().unwrap().unwrap(),
        };
        let mut followers = Vec::new();
        loop {
            match parse.next() {
                Some(Some(id)) => {
                    followers.push(Follower {
                        id,
                        signalled: parse.next().unwrap().unwrap(),
                        awake: parse.next().unwrap().unwrap(),
                        end: parse.next().unwrap().unwrap(),
                        latency: parse.next().unwrap().unwrap(),
                        duration: parse.next().unwrap().unwrap(),
                        status: parse.next().unwrap().unwrap(),
                        reserved: parse.next().unwrap().unwrap(),
                    });
                }
                Some(None) => {
                    for _ in 0..7 {
                        parse.next();
                    }
                }
                None => break,
            }
        }

        let mut frame = Frame { driver, followers };
        if frame
            .followers
            .iter()
            .any(|f| f.end > frame.driver.estimated_period)
        {
            println!("== Frame: Missed == ");
        } else {
            println!("== Frame == ");
        }
        frame
            .followers
            .sort_by(|a, b| a.signalled.cmp(&b.signalled));
        for f in frame.followers {
            println!(
                "{} <- end -- signalled: {:6} awake: {:6} done: {:6} -- duration: {:6} latency: {:6}  - id: {}",
                f.end, f.signalled, f.awake, f.end, f.duration, f.latency, f.id
            );
        }
        //}
    }

    Ok(())
}
